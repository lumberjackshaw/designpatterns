using System.Windows.Controls;

namespace Mediator
{
    public class DisplayLabel : Label
    {
        private readonly IMediator mediator;

        public DisplayLabel(IMediator mediator)
        {
            Content = "Starting ...";
            FontSize = 24;
            this.mediator = mediator;
            this.mediator.RegisterDisplay(this);
        }
    }
}