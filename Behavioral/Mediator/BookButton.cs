using System.Windows.Controls;

namespace Mediator
{
    public class BookButton : Button, IColleague
    {
        private readonly IMediator mediator;

        public BookButton(IMediator mediator)
        {
            Content = "Book";
            this.mediator = mediator;
            this.mediator.RegisterBook(this);
        }

        public void Execute()
        {
            mediator.Book();
        }
    }
}