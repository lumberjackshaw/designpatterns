namespace Mediator
{
    internal class Mediator : IMediator
    {
        private BookButton bookButton;
        private DisplayLabel displayLabel;
        private SearchButton searchButton;
        private ViewButton viewButton;

        public void Book()
        {
            bookButton.IsEnabled = false;
            viewButton.IsEnabled = true;
            searchButton.IsEnabled = true;
            displayLabel.Content = "Booking ...";
        }

        public void RegisterBook(BookButton bookButton)
        {
            this.bookButton = bookButton;
        }

        public void RegisterDisplay(DisplayLabel displayLabel)
        {
            this.displayLabel = displayLabel;
        }

        public void RegisterSearch(SearchButton searchButton)
        {
            this.searchButton = searchButton;
        }

        public void RegisterView(ViewButton viewButton)
        {
            this.viewButton = viewButton;
        }

        public void Search()
        {
            bookButton.IsEnabled = true;
            viewButton.IsEnabled = true;
            searchButton.IsEnabled = false;
            displayLabel.Content = "Searching ...";
        }

        public void View()
        {
            bookButton.IsEnabled = true;
            viewButton.IsEnabled = false;
            searchButton.IsEnabled = true;
            displayLabel.Content = "Viewing ...";
        }
    }
}