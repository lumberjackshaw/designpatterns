﻿/* GoF Mediator
 * Define an object that encapsulates how a set of objects interact. Mediator promotes loose coupling by keeping objects
 * from referring to each other explicitly, and lets you vary their interaction independently.
 */

using System;
using System.Windows;
using System.Windows.Controls;

namespace Mediator
{
    internal class MediatorDemo : Window
    {
        private readonly IMediator mediator = new Mediator();

        public MediatorDemo()
        {
            var rootView = new Grid();
            rootView.RowDefinitions.Add(new RowDefinition());
            rootView.RowDefinitions.Add(new RowDefinition());
            rootView.ColumnDefinitions.Add(new ColumnDefinition());
            rootView.ColumnDefinitions.Add(new ColumnDefinition());
            rootView.ColumnDefinitions.Add(new ColumnDefinition());

            var viewButton = new ViewButton(mediator);
            viewButton.Click += OnViewClick;
            Grid.SetColumn(viewButton, 0);
            Grid.SetRow(viewButton, 0);
            rootView.Children.Add(viewButton);

            var bookButton = new BookButton(mediator);
            bookButton.Click += OnViewClick;
            Grid.SetColumn(bookButton, 1);
            Grid.SetRow(bookButton, 0);
            rootView.Children.Add(bookButton);

            var searchButton = new SearchButton(mediator);
            searchButton.Click += OnViewClick;
            Grid.SetColumn(searchButton, 2);
            Grid.SetRow(searchButton, 0);
            rootView.Children.Add(searchButton);

            var displayLabel = new DisplayLabel(mediator);
            Grid.SetColumn(displayLabel, 0);
            Grid.SetRow(displayLabel, 1);
            Grid.SetColumnSpan(displayLabel, 3);
            rootView.Children.Add(displayLabel);

            Content = rootView;

            Width = 400;
            Height = 200;
            Title = "Mediator Demo";
        }

        private static void AppOnStartup(object sender, StartupEventArgs startupEventArgs)
        {
            Application.Current.MainWindow = new MediatorDemo();
            Application.Current.MainWindow.Show();
        }

        [STAThread]
        private static void Main(string[] args)
        {
            var app = new Application();
            app.Startup += AppOnStartup;
            app.Run();
        }

        private void OnViewClick(object sender, RoutedEventArgs routedEventArgs)
        {
            var colleague = (IColleague)sender;
            colleague.Execute();
        }
    }
}