using System.Windows.Controls;

namespace Mediator
{
    public class SearchButton : Button, IColleague
    {
        private readonly IMediator mediator;

        public SearchButton(IMediator mediator)
        {
            Content = "Search";
            this.mediator = mediator;
            this.mediator.RegisterSearch(this);
        }

        public void Execute()
        {
            mediator.Search();
        }
    }
}