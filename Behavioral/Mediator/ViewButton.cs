using System.Windows.Controls;

namespace Mediator
{
    public class ViewButton : Button, IColleague
    {
        private readonly IMediator mediator;

        public ViewButton(IMediator mediator)
        {
            Content = "View";
            this.mediator = mediator;
            this.mediator.RegisterView(this);
        }

        public void Execute()
        {
            mediator.View();
        }
    }
}