﻿namespace Memento
{

    public partial class Originator {
        public class Momento {
			private readonly string state;

			internal Momento(string state) {
				this.state = state;
			}

			public string State {
				get {
					return this.state;
				}
			}
		}
	}
}