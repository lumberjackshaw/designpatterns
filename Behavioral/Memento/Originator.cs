﻿using System;

namespace Memento
{
    public partial class Originator
    {
        private string state;

        public void RestoreFromMomento(Momento momento)
        {
            state = momento.State;
            Console.WriteLine("Originator: resotring from momento {0}", this.state);
        }

        public Momento SaveToMomento()
        {
            Console.WriteLine("Originator: saving to momento...");
            return new Originator.Momento(state);
        }

        public void SetState(string state)
        {
            Console.WriteLine("Originator: setting momento to {0}", state);
            this.state = state;
        }
    }
}